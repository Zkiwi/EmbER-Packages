################################################################################
#
# ember_sickrage
#
################################################################################

EMBER_SICKRAGE_VERSION = v5.1.2
EMBER_SICKRAGE_SITE = git@github.com:SickRage/SickRage.git
EMBER_SICKRAGE_SITE_METHOD = git
EMBER_SICKRAGE_INSTALL_STAGING = NO
EMBER_SICKRAGE_INSTALL_TARGET = YES

EMBER_SICKRAGE_PK_NAME = "SickRage"
EMBER_SICKRAGE_PK_VERSION = 4.0.75
EMBER_SICKRAGE_PK_DESCRIPTION = "Video File Manager for TV Shows, It watches for new episodes of your favorite shows and when they are posted it does its magic. (Default Port: 8081)"
EMBER_SICKRAGE_PK_DEPENDENCIES = "PythonLibs"
EMBER_SICKRAGE_PK_REQUIREDFW = "2.0.0"
EMBER_SICKRAGE_PK_DOWNLOAD = "https://bitbucket.org/ember-dev/ember-packages/raw/master/PACKAGES-armv7l/package-SickRage.tar.gz"
EMBER_SICKRAGE_PK_INITSCRIPT = "PSickRage"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_SICKRAGE_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info
	echo 'name = $(EMBER_SICKRAGE_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info
	echo 'description = $(EMBER_SICKRAGE_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info
	echo 'version = "$(EMBER_SICKRAGE_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_SICKRAGE_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_SICKRAGE_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info
	echo 'download = $(EMBER_SICKRAGE_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/package_info

	cp -rf $(@D)/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))
	cp -rfv $(BR2_EXTERNAL)/package/ember_sickrage/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Programs/$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_sickrage/$(call qstrip,$(EMBER_SICKRAGE_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SICKRAGE_PK_NAME))/Run/$(call qstrip,$(EMBER_SICKRAGE_PK_INITSCRIPT))
endef

$(eval $(generic-package))
